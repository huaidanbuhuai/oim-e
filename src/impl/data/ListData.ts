import NodeData from '@/views/common/list/NodeData';

class ListData {
    public static userNodes: NodeData[] = [];
    public static groupNodes: NodeData[] = [];
}

export default ListData;
