export default class MessagePaneType {

    public TYPE_NO: string = 'no';
    public TYPE_USER_CHAT: string = 'user_chat';
    public TYPE_GROUP_CHAT: string = 'group_chat';
    public TYPE_APPLY_HANDLE: string = 'apply_handle';
}
